#!/bin/bash

echo "deb https://download.gocd.org /" | tee /etc/apt/sources.list.d/gocd.list
curl https://download.gocd.org/GOCD-GPG-KEY.asc | apt-key add -
apt-get update

apt-get install -y go-agent

sed -i 's/wrapper.app.parameter.101=http:\/\/localhost:8153\/go/wrapper.app.parameter.101=http:\/\/10.0.0.10:8153\/go/g' /usr/share/go-agent/wrapper-config/wrapper-properties.conf

systemctl enable go-agent
systemctl start go-agent