#!/bin/bash

echo "deb https://download.gocd.org /" | tee /etc/apt/sources.list.d/gocd.list
curl https://download.gocd.org/GOCD-GPG-KEY.asc | apt-key add -
apt-get update

apt-get install -y apache2-utils go-server

systemctl enable go-server
systemctl start go-server